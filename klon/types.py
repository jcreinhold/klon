#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
klon.types

define common types used in klon

Author: Jacob Reinhold (jacob.reinhold@jhu.edu)

Created on: Mar 6, 2019
"""

import numpy as np


Img = np.ndarray
