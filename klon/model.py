#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
klon.atlas

define data structures for images

Author: Jacob Reinhold (jacob.reinhold@jhu.edu)

Created on: Mar 6, 2019
"""

__all__ = ['Params',
           'MeanNMR',
           'QuantAtlas',
           'Kloner']

from concurrent.futures import ProcessPoolExecutor
from dataclasses import dataclass
from functools import partial
import logging
from typing import List, Tuple

import nibabel as nib
import numpy as np
from scipy.optimize import least_squares, minimize, LinearConstraint

from .atlas import Atlas, MRI
from .types import Img

EPS = 1e-6

logger = logging.getLogger(__name__)


@dataclass
class Params:
    params: Tuple[float, float, float]
    ps: str

    def __getitem__(self, idx):
        return np.squeeze(self.params[idx])


@dataclass
class MeanNMR:
    """
    mean nuclear magnetic resonance (relaxation) parameters for t1, t2 and pd
    for cerebro-spinal fluid, gray matter and white matter, respectively
    """
    t1: Params = Params((4550, 1330, 830), 't1')
    t2: Params = Params((1200, 102, 86), 't2')
    pd: Params = Params((1.0, 0.86, 0.77), 'pd')


# Equations for generate_fast (fixed for MPRAGE, T2-w, PD-w input)
def F(x, θ, s):
    t1, t2, pd = x
    θmp, θt2, θpd = θ
    out = np.squeeze(3 * pd + θmp[0] + θt2[0] + θpd[0] + t1 * (θmp[1] + θt2[1] + θpd[1]) + \
          (t1 ** 2 * θmp[2]) + (θt2[2] + θpd[2]) / t2 - s)
    return out


def dF(x, θ, s):
    t1, t2, pd = x
    θmp, θt2, θpd = θ
    out = np.zeros_like(x)
    out[0] = (θmp[1] + θt2[1] + θpd[1]) + (2 * t1 * θmp[2])
    out[1] = -(θt2[2] + θpd[2])/t2**2
    out[2] = 3
    return out


def H(x, θ, s):
    t1, t2, pd = x
    θmp, θt2, θpd = θ
    out = np.diag((2 * θmp[2], 2 * (θt2[2] + θpd[2])/t2**3, 0))
    return out


def minimizer(args, x0, constr, θ):
    s, i = args
    return minimize(fun=F, x0=x0, constraints=constr, method='trust-constr', jac=dF, hess=H, args=(θ, s)).x, i


class QuantAtlas(Atlas):

    @classmethod
    def generate(cls, atlas:Atlas, nmr:Params=None):
        nmr = nmr or MeanNMR()
        solver = SolveImagingParams(nmr)
        x0 = (nmr.t1[1], nmr.t2[1], np.log(nmr.pd[1]))
        bounds = ([EPS, EPS, np.log(EPS)], [1e6, 5e5, 0.])
        t1q, t2q, pdq = [], [], []
        pcts = np.arange(0, 101, 5)
        for i, (t1, t2, pd) in enumerate(zip(atlas.t1, atlas.t2, atlas.pd), 1):
            θ = (solver(t1), solver(t2), solver(pd))
            # solve non-linear system of equations to get quantitiative maps
            mask = (t1.img > EPS) & (t2.img > EPS) & (pd.img > EPS)
            xhat_ = []
            pct_idxs = np.floor(np.percentile(np.arange(np.sum(mask)), pcts))
            S = (np.log(t1[mask]), np.log(t2[mask]), np.log(pd[mask]))
            for j, (st1, st2, spd) in enumerate(zip(*S), 1):
                if np.sum(pct_idxs==j) > 0:
                    logger.info(f'{int(pcts[pct_idxs==j])}% done with image ({i}/{len(atlas)})')
                def f(x):
                    t1_, t2_, pd_ = x
                    out = (SolveQuant(t1.pulse_sequence)(θ[0], st1, t1_, t2_, pd_),
                           SolveQuant(t2.pulse_sequence)(θ[1], st2, t1_, t2_, pd_),
                           SolveQuant(pd.pulse_sequence)(θ[2], spd, t1_, t2_, pd_))
                    return np.asarray(out)
                def df(x):
                    t1_, t2_, pd_ = x
                    out = (SolveQuantGrad(t1.pulse_sequence)(θ[0], st1, t1_, t2_, pd_),
                           SolveQuantGrad(t2.pulse_sequence)(θ[1], st2, t1_, t2_, pd_),
                           SolveQuantGrad(pd.pulse_sequence)(θ[2], spd, t1_, t2_, pd_))
                    return np.vstack(out)
                res = least_squares(f, x0, bounds=bounds, jac=df)
                xhat_.append(res.x)
            xhat = np.vstack(xhat_)
            t1q_, t2q_, pdq_ = np.zeros_like(t1.img), np.zeros_like(t2.img), np.zeros_like(pd.img)
            t1q_[mask], t2q_[mask], pdq_[mask] = xhat[:,0], xhat[:,1], np.exp(xhat[:,2])
            t1q.append(MRI.from_mri(t1q_.copy(), t1))
            t2q.append(MRI.from_mri(t2q_.copy(), t2))
            pdq.append(MRI.from_mri(pdq_.copy(), pd))
        return cls(t1q, t2q, pdq)

    @classmethod
    def generate_fast(cls, atlas:Atlas, nmr:Params=None, num_workers:int=None):
        nmr = nmr or MeanNMR()
        solver = SolveImagingParams(nmr)
        x0 = (nmr.t1[1], nmr.t2[1], np.log(nmr.pd[1]))
        bounds = ([EPS, EPS, np.log(EPS)], [1e6, 5e5, 0.])
        constr = LinearConstraint(np.eye(3), bounds[0], bounds[1])
        #bounds = ((EPS, 1e6), (EPS, 5e5), (np.log(EPS),0.))
        t1q, t2q, pdq = [], [], []
        for i, (t1, t2, pd) in enumerate(zip(atlas.t1, atlas.t2, atlas.pd), 1):
            θ = (solver(t1), solver(t2), solver(pd))
            mask = (t1.img > EPS) & (t2.img > EPS) & (pd.img > EPS)
            S = np.log(t1[mask]) + np.log(t2[mask]) + np.log(pd[mask])
            N = len(S)
            I = range(N)
            f = partial(minimizer, x0=x0, constr=constr, θ=θ)
            with ProcessPoolExecutor(max_workers=num_workers) as executor:
                o = [res for res in executor.map(f, zip(S,I), chunksize=max(N//1000,1))]
            print(o)
            xhat = np.vstack([x[0] for x in sorted(o, key=lambda x: x[1])])
            t1q_, t2q_, pdq_ = np.zeros_like(t1.img), np.zeros_like(t2.img), np.zeros_like(pd.img)
            t1q_[mask], t2q_[mask], pdq_[mask] = xhat[:,0], xhat[:,1], np.exp(xhat[:,2])
            t1q.append(MRI.from_mri(t1q_.copy(), t1))
            t2q.append(MRI.from_mri(t2q_.copy(), t2))
            pdq.append(MRI.from_mri(pdq_.copy(), pd))
        return cls(t1q, t2q, pdq)

    @classmethod
    def from_fns(cls, t1_fns:List[str],t2_fns: List[str], pd_fns:List[str], **kwargs):
        t1s = [nib.load(fn) for fn in t1_fns]
        t2s = [nib.load(fn) for fn in t2_fns]
        pds = [nib.load(fn) for fn in pd_fns]
        t1s = [MRI(t1.get_data(), t1.affine, t1.header) for t1 in t1s]
        t2s = [MRI(t2.get_data(), t2.affine, t2.header) for t2 in t2s]
        pds = [MRI(pd.get_data(), pd.affine, pd.header) for pd in pds]
        return cls(t1s, t2s, pds)


class PulseSequence:
    """ base class for all supported pulse sequences """

    def __init__(self, ps:str):
        if ps.lower() == 'flash':
            self.eqn = self.flash
        elif ps.lower() == 'mprage':
            self.eqn = self.mprage
        elif ps.lower() == 't2' or ps.lower() == 't2space':
            self.eqn = self.t2space
        elif ps.lower() == 'pd':
            # approximate PD equation is the T2-SPACE equation
            self.eqn = self.t2space
        else:
            raise ValueError(f'{ps} either not valid or not supported')

    def __call__(self, *args, **kwargs):
        return self.eqn(*args, **kwargs)

    def flash(self, *args, **kwargs):
        raise NotImplementedError

    def mprage(self, *args, **kwargs):
        raise NotImplementedError

    def t2space(self, *args, **kwargs):
        raise NotImplementedError


class SolveQuant(PulseSequence):
    """
    contains all the supported signal equations,
    used for solving for quant (s is the intensity value in the underlying image)
    here we solve for log(pd) using log(s)
    """
    @staticmethod
    def flash(θ:Params, s:float, t1:float, t2:float, pd:float):
        """ t1,t2,pd are quantitative values """
        return (pd + θ[0] + θ[1] / t1 + θ[2] / t2) - s

    @staticmethod
    def mprage(θ:Params, s:float, t1:float, t2:float, pd:float):
        """ t1,t2,pd are quantitative values """
        return (pd + θ[0] + θ[1] * t1 + θ[2] * (t1 ** 2)) - s

    @staticmethod
    def t2space(θ:Params, s:float, t1:float, t2:float, pd:float):
        """ t1,t2,pd are quantitative values """
        return (pd + θ[0] + θ[1] * t1 + θ[2] / t2) - s


class SolveQuantGrad(PulseSequence):
    """ gradients for the equations in SolveQuantitative """
    @staticmethod
    def flash(θ:Params, s:float, t1:float, t2:float, pd:float):
        return (-θ[1]/t1**2, -θ[2]/t2**2, 1)

    @staticmethod
    def mprage(θ:Params, s:float, t1:float, t2:float, pd:float):
        return (θ[1] + 2 * θ[2] * t1, 0, 1)

    @staticmethod
    def t2space(θ:Params, s:float, t1:float, t2:float, pd:float):
        return (θ[1], -θ[2]/t2**2, 1)


class SignalEquations(PulseSequence):
    """ contains all the supported signal equations """

    @staticmethod
    def flash(θ:Params, t1:Img, t2:Img, pd:Img):
        """ t1,t2,pd are quantitative maps """
        return np.exp(np.log(pd) + θ[0] + θ[1] / t1 + θ[2] / t2)

    @staticmethod
    def mprage(θ:Params, t1:Img, t2:Img, pd:Img):
        """ t1,t2,pd are quantitative maps """
        return np.exp(np.log(pd) + θ[0] + θ[1] * t1 + θ[2] * (t1 ** 2))

    @staticmethod
    def t2space(θ:Params, t1:Img, t2:Img, pd:Img):
        """ t1,t2,pd are quantitative maps """
        return np.exp(np.log(pd) + θ[0] + θ[1] * t1 + θ[2] / t2)


class BMatrix(PulseSequence):
    """ contains all the supported B matrices """

    @staticmethod
    def flash(*args):
        t1c, t1g, t1w, t2c, t2g, t2w = args
        B = np.array([[1., 1. / t1c, 1. / t2c],
                      [1., 1. / t1g, 1. / t2g],
                      [1., 1. / t1w, 1. / t2w]])
        return B

    @staticmethod
    def mprage(*args):
        t1c, t1g, t1w, *_ = args
        B = np.array([[1.0, t1c, t1c ** 2],
                      [1.0, t1g, t1g ** 2],
                      [1.0, t1w, t1w ** 2]])
        return B

    @staticmethod
    def t2space(*args):
        t1c, t1g, t1w, t2c, t2g, t2w = args
        B = np.array([[1.0, t1c, 1. / t2c],
                      [1.0, t1g, 1. / t2g],
                      [1.0, t1w, 1. / t2w]])
        return B


class SolveImagingParams:
    def __init__(self, mean_nmr:MeanNMR=None):
        self.nmr = mean_nmr or MeanNMR()

    def __call__(self, x:MRI):
        logsc, logsg, logsw = x.mean_tissue_intensity()
        logpdc, logpdg, logpdw = np.log((*self.nmr.pd,))
        B = BMatrix(x.pulse_sequence)(*self.nmr.t1, *self.nmr.t2)
        s = np.array([[logsc - logpdc],
                      [logsg - logpdg],
                      [logsw - logpdw]])
        θ = np.linalg.solve(B, s)
        θ = Params(θ, x.pulse_sequence)
        return θ


class GenerateImages:
    def __init__(self, qatlas:QuantAtlas):
        self.qatlas = qatlas

    def __call__(self, θ:Params, idx:int):
        pdi, t1i, t2i, fg = self.qatlas.pd[idx], self.qatlas.t1[idx], self.qatlas.t2[idx], self.qatlas.fg[idx]
        pd, t1, t2 = pdi[fg], t1i[fg], t2i[fg]
        syn = np.zeros_like(pd)
        syn[fg] = SignalEquations(θ.ps)(θ, t1, t2, pd)
        return MRI(syn, t1i.affine, t1i.header)


class Kloner:
    def __init__(self, qatlas:QuantAtlas, mean_nmr:MeanNMR=None):
        self.param_solver = SolveImagingParams(mean_nmr)
        self.img_solver = GenerateImages(qatlas)

    def apply(self, θ:Params, idx:int):
        return self.img_solver(θ, idx)

    def solve(self, x:MRI):
        return self.param_solver(x)

    def __len__(self): return len(self.img_solver.qatlas)
