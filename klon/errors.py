#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
klon.errors

This module holds project defined errors

Author: Jacob Reinhold (jacob.reinhold@jhu.edu)

Created on: Mar 6, 2018
"""


class KlonError(Exception):
    pass
