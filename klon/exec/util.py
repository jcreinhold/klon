#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
klon.exec.util

helper functions for executables

Author: Jacob Reinhold (jacob.reinhold@jhu.edu)

Created on: Mar 6, 2019
"""

__all__ = ['setup_log']

from typing import List, Tuple

from glob import glob
import logging
import os

logger = logging.getLogger(__name__)


def setup_log(verbosity):
    """ get logger with appropriate logging level and message """
    if verbosity == 1:
        level = logging.getLevelName('INFO')
    elif verbosity >= 2:
        level = logging.getLevelName('DEBUG')
    else:
        level = logging.getLevelName('WARNING')
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=level)


def split_filename(filepath: str) -> Tuple[str,str,str]:
    """ split a filepath into the directory, base, and extension """
    path = os.path.dirname(filepath)
    filename = os.path.basename(filepath)
    base, ext = os.path.splitext(filename)
    if ext == '.gz':
        base, ext2 = os.path.splitext(base)
        ext = ext2 + ext
    return path, base, ext


def glob_imgs(path: str, ext='*.nii*') -> List[str]:
    """ grab all `ext` files in a directory and sort them for consistency """
    fns = sorted(glob(os.path.join(path, ext)))
    return fns