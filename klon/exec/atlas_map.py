#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
klon.exec.atlas_map

command line interface to generate atlas images in the
style of a set of images from the atlas beta space maps

Author: Jacob Reinhold (jacob.reinhold@jhu.edu)

Created on: Mar 6, 2019
"""

import argparse
import logging
import os
import sys
import warnings

with warnings.catch_warnings():
    warnings.filterwarnings('ignore', category=FutureWarning)
    warnings.filterwarnings('ignore', category=UserWarning)
    import nibabel as nib
    import numpy as np
    from ..atlas import MRI
    from ..errors import KlonError
    from ..model import Kloner, QuantAtlas
    from .util import glob_imgs, setup_log


######## Helper functions ########

def arg_parser():
    parser = argparse.ArgumentParser(description='create atlas images in the style of a given image contrast')

    required = parser.add_argument_group('Required')
    required.add_argument('-i', '--img-dir', type=str, required=True,
                          help='path to directory with source images')
    required.add_argument('-p', '--pulse-sequence', type=str, required=True,
                          help='pulse sequences of the image directory')
    required.add_argument('-b', '--beta-dir', type=str, required=True, nargs=3,
                          help='path to directory with beta map images (three paths for each type of beta image)')
    required.add_argument('-o', '--out-dir', type=str, required=True,
                          help='path to output the beta space maps of the images')

    options = parser.add_argument_group('Options')
    options.add_argument('--seed', type=int, default=0)
    required.add_argument('-t', '--tissue-masks', type=str, default=None,
                          help='path to directory with corresponding tissue masks')
    options.add_argument('-v', '--verbosity', action="count", default=0,
                         help="increase output verbosity (e.g., -vv is more than -v)")
    return parser


######### Main routine ###########

def main(args=None):
    args = arg_parser().parse_args(args)
    setup_log(args.verbosity)
    logger = logging.getLogger(__name__)
    try:
        # set random seed for reproducibility
        np.random.seed(args.seed)
        fns = glob_imgs(args.img_dir)
        beta_fns = [glob_imgs(imd) for imd in args.beta_dir]
        tm_fns = [None] * len(fns) if args.tissue_masks is None else glob_imgs(args.tissue_masks)
        qatlas = QuantAtlas.from_fns(*beta_fns)
        kloner = Kloner(qatlas)
        for fn, tm_fn in zip(fns, tm_fns):
            img = nib.load(fn)
            if args.pulse_sequence == 'mprage' or args.pulse_sequence == 'flash':
                img = MRI.from_t1(img.get_data(), args.pulse_sequence, img.affine, img.header)
            elif args.tissue_mask is not None:
                tm = nib.load(tm_fn).get_data()
                img = MRI(img.get_data(), img.affine, img.header, tm > 0, tm, args.pulse_sequence)
            else:
                raise KlonError('If pulse sequence != mprage or flash, then must provide tissue mask.')
            θ = kloner.solve(img)
            for i in range(len(kloner)):
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    out = kloner.apply(θ, i)
                out.save(os.path.join(args.out_dir, f'atlas_{i}.nii.gz'))
        return 0
    except Exception as e:
        logger.exception(e)
        return 1


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
