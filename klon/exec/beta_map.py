#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
klon.exec.beta_map

command line interface to generate beta space (pseudo-quantitative) atlases

Author: Jacob Reinhold (jacob.reinhold@jhu.edu)

Created on: Mar 6, 2019
"""

import argparse
import logging
import sys
import warnings

with warnings.catch_warnings():
    warnings.filterwarnings('ignore', category=FutureWarning)
    warnings.filterwarnings('ignore', category=UserWarning)
    import numpy as np
    from ..atlas import Atlas
    from ..model import QuantAtlas
    from .util import glob_imgs, setup_log


######## Helper functions ########

def arg_parser():
    parser = argparse.ArgumentParser(description='create beta map images from a set of images')

    required = parser.add_argument_group('Required')
    required.add_argument('-i', '--img-dir', type=str, required=True, nargs=3,
                          help='path to directory with source images (multiple paths for multiple types of images)')
    required.add_argument('-p', '--pulse-seqs', type=str, required=True, nargs=3,
                          help='pulse sequences of the three input image directories')
    required.add_argument('-o', '--out-dir', type=str, required=True,
                          help='path to output the beta space maps of the images')

    options = parser.add_argument_group('Options')
    options.add_argument('--seed', type=int, default=0)
    required.add_argument('-f', '--fast', action='store_true', default=False,
                          help='use fast method, assumes pulse sequences are: mprage, t2-space and pd')
    options.add_argument('-v', '--verbosity', action="count", default=0,
                         help="increase output verbosity (e.g., -vv is more than -v)")
    return parser


######### Main routine ###########

def main(args=None):
    args = arg_parser().parse_args(args)
    setup_log(args.verbosity)
    logger = logging.getLogger(__name__)
    try:
        # set random seed for reproducibility
        np.random.seed(args.seed)
        fns = [glob_imgs(imd) for imd in args.img_dir]
        atlas = Atlas.from_fns(*fns, *args.pulse_seqs)
        qatlas = QuantAtlas.generate(atlas) if not args.fast else QuantAtlas.generate_fast(atlas)
        qatlas.save(args.out_dir)
        return 0
    except Exception as e:
        logger.exception(e)
        return 1


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
