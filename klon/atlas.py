#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
klon.atlas

define data structures for images

Author: Jacob Reinhold (jacob.reinhold@jhu.edu)

Created on: Mar 6, 2019
"""

__all__ = ['MRI',
           'Atlas',
           'calc_brain_mask',
           'calc_tissue_mask']


from dataclasses import dataclass
import logging
import os
from typing import Any, Optional, List

from scipy.ndimage.morphology import binary_fill_holes
from sklearn.mixture import GaussianMixture
import nibabel as nib
import numpy as np

from .types import Img

logger = logging.getLogger(__name__)


@dataclass
class MRI:
    """
    holds an MR brain image, its brain mask and its tissue mask
    and a record of the pulse sequence
    """
    img: Img
    affine: Any
    header: Any
    brain_mask: Optional[Img] = None
    tissue_mask: Optional[Img] = None
    pulse_sequence: Optional[str] = None

    def __getitem__(self, idx):
        return self.img[idx]

    @classmethod
    def from_t1(cls, img:Img, ps:str, affine, header, t:float=0):
        brain_mask = calc_brain_mask(img, t)
        tissue_mask = calc_tissue_mask(img, brain_mask)
        return cls(img, affine, header, brain_mask, tissue_mask, ps)

    @classmethod
    def from_mri(cls, img:Img, mri, ps:str=None, affine=None, header=None):
        """ mri is an instance of this class """
        ps = ps or mri.pulse_sequence
        affine = affine if affine is not None else mri.affine
        header= header or mri.header
        return cls(img, affine, header, mri.brain_mask, mri.tissue_mask, ps)

    @classmethod
    def open(cls, fn, **kwargs):
        img = nib.load(fn)
        return cls(img.get_data(), img.affine, img.header, **kwargs)

    def mean_tissue_intensity(self, median:bool=True, log:bool=True):
        csf, gm, wm = [self.img[self.tissue_mask == i] for i in range(1, 4)]
        sc = np.median(csf) if median else np.mean(csf)
        sg = np.median(gm) if median else np.mean(gm)
        sw = np.median(wm) if median else np.mean(wm)
        logger.debug(f'CSF: {sc:0.2e}, GM: {sg:0.2e}, WM: {sw:0.2e}')
        if log: sc, sg, sw = np.log(sc), np.log(sg), np.log(sw)
        return sc, sg, sw

    def save(self, fn):
        nib.Nifti1Image(self.img, self.affine, self.header).to_filename(fn)


@dataclass
class Atlas:
    """ set of t1, t2 and pd imgs forming the atlas """
    t1: List[MRI]
    t2: List[MRI]
    pd: List[MRI]

    @property
    def fg(self): return [t1.brain_mask for t1 in self.t1]

    @classmethod
    def from_fns(cls, t1_fns:List[str],t2_fns: List[str], pd_fns:List[str],
                 t1_ps:str, t2_ps:str, pd_ps:str):
        t1s = [nib.load(fn) for fn in t1_fns]
        t2s = [nib.load(fn) for fn in t2_fns]
        pds = [nib.load(fn) for fn in pd_fns]
        t1s = [MRI.from_t1(t1.get_data(), t1_ps, t1.affine, t1.header) for t1 in t1s]
        t2s = [MRI.from_mri(t2.get_data(), t1, t2_ps, t2.affine, t2.header) for t1, t2 in zip(t1s, t2s)]
        pds = [MRI.from_mri(pd.get_data(), t1, pd_ps, pd.affine, pd.header) for t1, pd in zip(t1s, pds)]
        return cls(t1s, t2s, pds)

    @classmethod
    def from_atlas(cls, atlas):
        return cls(atlas.t1, atlas.t2, atlas.pd)

    def save(self, out_dir:str):
        dirs = [os.path.join(out_dir, t) for t in ('t1', 't2', 'pd')]
        for d in dirs: os.makedirs(d, exist_ok=True)
        for i, (t1, t2, pd) in enumerate(zip(self.t1, self.t2, self.pd), 1):
            fns = [os.path.join(d, t + f'_{i}.nii.gz') for d, t in zip(dirs, ('t1', 't2', 'pd'))]
            for f, im in zip(fns, (t1, t2, pd)):
                logger.info(f'Saving image: {f}.')
                im.save(f)

    def __getitem__(self, item):
        return self.t1[item], self.t2[item], self.pd[item]

    def __len__(self): return len(self.t1)


def calc_brain_mask(img:Img, t:float=None, fill_holes:bool=True) -> Img:
    """ calculate a brain mask by thresholding (assume skull-stripped) """
    brain_mask = img > img.mean() if t is None else img > t
    # fill holes defaults to false because some processing takes log, problematic for 0 intensities
    if fill_holes: brain_mask = binary_fill_holes(brain_mask)
    return brain_mask


def calc_tissue_mask(t1:Img, brain_mask:Img) -> Img:
    """ calculate a tissue mask with a (non-contrast enhanced) t1-w image """
    brain = np.expand_dims(t1[brain_mask].flatten(), 1)
    gmm = GaussianMixture(3)
    gmm.fit(brain)
    classes = np.argsort(gmm.means_.squeeze())
    tmp_predicted = gmm.predict(brain)
    predicted = np.zeros(tmp_predicted.shape)
    for i, c in enumerate(classes):
        predicted[tmp_predicted == c] = i + 1
    tissue_mask = np.zeros(t1.shape)
    tissue_mask[brain_mask] = predicted
    return tissue_mask
