#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
setup

Module installs the klon package
Can be run via command: python setup.py install (or develop)

Author: Jacob Reinhold (jacob.reinhold@jhu.edu)

Created on: Mar 6, 2019
"""

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

version = '0.0.1'

args = dict(
    name='klon',
    version=version,
    description="model-based style transfer for MR images",
    long_description=readme,
    author='Jacob Reinhold',
    author_email='jacob.reinhold@jhu.edu',
    url='https://gitlab.com/jcreinhold/klon',
    license=license,
    packages=find_packages(exclude=('tests', 'docs', 'tutorials')),
    keywords="mr image-synthesis psiclone",
    entry_points={
        'console_scripts': ['atlas-map=klon.exec.atlas_map:main',
                            'beta-map=klon.exec.beta_map:main']
    })

setup(install_requires=['nibabel>=2.3.1',
                        'numpy>=1.15.4',
                        'scikit-learn',
                        'scipy'], **args)
