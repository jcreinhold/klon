#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
tests.test_exec

test the klon command line interfaces for runtime errors

Author: Jacob Reinhold (jacob.reinhold@jhu.edu)

Created on: Mar 06, 2019
"""

import os
import shutil
import tempfile
import unittest

from klon.exec.atlas_map import main as atlas_map
from klon.exec.beta_map import main as beta_map


class TestCLI(unittest.TestCase):

    def setUp(self):
        wd = os.path.dirname(os.path.abspath(__file__))
        self.nii_dir = os.path.join(wd, 'test_data')
        self.out_dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.out_dir)


class TestBetaMap(TestCLI):

    def test_atlas_map(self):
        args = ['-i'] + [f'{self.nii_dir}'] + ['-p', 'mprage', '-b'] + [f'{self.nii_dir}'] * 3 + ['-o'] + [f'{self.out_dir}']
        retval = atlas_map(args)
        self.assertEqual(0, retval)

    def test_beta_map(self):
        args = ['-i'] + [f'{self.nii_dir}'] * 3 + ['-p', 'mprage', 't2', 'pd'] + ['-o'] + [f'{self.out_dir}']
        retval = beta_map(args)
        self.assertEqual(0, retval)

    def test_beta_map_fast(self):
        args = ['-i'] + [f'{self.nii_dir}'] * 3 + ['-p', 'mprage', 't2', 'pd'] + ['-o'] + [f'{self.out_dir}'] + ['-f']
        retval = beta_map(args)
        self.assertEqual(0, retval)


if __name__ == '__main__':
    unittest.main()
