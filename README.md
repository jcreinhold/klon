klon
=======================

<!--
[![Build Status](https://travis-ci.org/jcreinhold/synthnn.svg?branch=master)](https://travis-ci.org/jcreinhold/synthnn)
[![Coverage Status](https://coveralls.io/repos/github/jcreinhold/synthnn/badge.svg?branch=master)](https://coveralls.io/github/jcreinhold/synthnn?branch=master)
[![Documentation Status](https://readthedocs.org/projects/synthnn/badge/?version=latest)](http://synthnn.readthedocs.io/en/latest/)
[![Docker Automated Build](https://img.shields.io/docker/build/jcreinhold/synthnn.svg)](https://hub.docker.com/r/jcreinhold/synthnn/)
[![Python Versions](https://img.shields.io/badge/python-3.6%20%7C%203.7-blue.svg)](https://www.python.org/downloads/release/python-360/)
[![DOI](https://zenodo.org/badge/155944524.svg)](https://zenodo.org/badge/latestdoi/155944524) -->

This package contains modules to conduct style transfer between MR images from different sets using a model of MR pulse sequences.

** Note that this is an **alpha** release. If you have feedback or problems, please submit an issue (it is very appreciated) **

This package was developed by [Jacob Reinhold](https://jcreinhold.github.io) and the other students and researchers of the 
[Image Analysis and Communication Lab (IACL)](http://iacl.ece.jhu.edu/index.php/Main_Page).

[Link to main Gitlab Repository](https://gitlab.com/jcreinhold/klon)

Requirements
------------

- nibabel >= 2.3.1
- numpy >= 1.15.4
- scikit-learn >= 0.20.1
- scipy >= 1.1.0

The above packages are required; however, previous versions *may* work (previous versions not tested nor supported).

Installation
------------

    pip install git+git://github.com/jcreinhold/klon.git

<!--
Tutorial
--------

[5 minute Overview](https://github.com/jcreinhold/synthnn/blob/master/tutorials/5min_tutorial.md)

[Jupyter Notebook example](https://nbviewer.jupyter.org/github/jcreinhold/synthnn/blob/master/tutorials/tutorial.ipynb)

In addition to the above small tutorial and example notebook, there is consolidated documentation [here](https://synthnn.readthedocs.io/en/latest/). -->

Test Package
------------

Unit tests can be run from the main directory as follows:

    nosetests -v tests

Relevant Papers
---------------

[1] Jog, Amod, et al. "MR image synthesis by contrast learning on neighborhood ensembles." Medical image analysis 24.1 (2015): 63-76.

[2] Jog, Amod, et al. "PSACNN: Pulse Sequence Resilient Fast Whole Brain Segmentation." arXiv preprint arXiv:1901.05992 (2019).
